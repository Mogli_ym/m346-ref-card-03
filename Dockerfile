# base image mit maven/eclipse/jdk17
 
FROM maven:3.9-eclipse-temurin-17-alpine
 
 
# Kopiere die beiden Codequellen
 
COPY pom.xml .
 
COPY src src
 
 
# maven build prozess
 
RUN mvn package
 
 
# Container Start-Befehl
 
ENTRYPOINT ["java","-jar","target/app-refcard-01-0.0.1-SNAPSHOT.jar"]